#include "tightBinding.h"


/*
 * Initializes the coupling consants
 */
TightBinding::TightBinding(const Par_t *p, const std::vector<double> x,
                   const std::vector<double> absorber) : param(p), x(x), absorber(absorber)
{
    int dotSize = p->grid.dotSize;
    int spaceSize = p->grid.spaceSize;
    int fullSize = dotSize + 2 * spaceSize;
    double a = p->dot.siteDistance;
    E_electron.resize(fullSize);
    E_hole.resize(dotSize);
    V.resize(fullSize);
    c_e.resize(fullSize-1);
    c_h.resize(dotSize-1);

    double base_c_e_dot = - 0.5 / p->dot.m_eff_electron / (a * a);
    double base_c_e_space = -0.5 / (p->grid.dx * p->grid.dx );
    double base_c_h = - 0.5 / p->dot.m_eff_hole / ( a * a );

    // free space coupling constant is used for coupling between dot and free space
    for(int i=0; i<spaceSize; i++){
        c_e[i] = base_c_e_space;
        c_e[i + dotSize + spaceSize -1] = base_c_e_space;
    }
    for(int i=0; i<dotSize-1; i++){
        double hSr = a/(x[spaceSize+i+1] - x[spaceSize+i]);
        c_e[i+spaceSize] = base_c_e_dot * hSr * hSr;
        c_h[i] = base_c_h * hSr * hSr;
    }
    // setup physical energies
    for(int i=0; i<spaceSize; i++){
        E_electron[i] = p->dot.E_ionization - p->dot.E_gap / 2;
        E_electron[i+spaceSize+dotSize] = E_electron[i];
    }
    for(int i=0; i<dotSize; i++){
        E_electron[spaceSize+i] = p->dot.E_gap / 2;
        E_hole[i] = p->dot.E_gap / 2;
    }
    // correct energies to simplify EOMs
    for(int i=0; i<fullSize-1; i++){
        E_electron[i] -= c_e[i];
        E_electron[i+1] -= c_e[i];
    }
    for(int i=0; i<dotSize-1; i++){
        E_hole[i] -= c_h[i];
        E_hole[i+1] -= c_h[i];
    }
    dipolmoment = p->dot.dipolmoment;

    // field induces potential
    double epsRatio = p->dot.epsilon_inside / p->dot.epsilon_outside;
    double dotRadius = (x[spaceSize+dotSize-1] - x[spaceSize] ) / 2;
    for(int i=0; i<fullSize; i++){
        if ( i>=spaceSize && i<spaceSize + dotSize ){
            V[i] = - 3 / ( epsRatio + 2) * x[i];
        } else {
            V[i] = - x[i] + x[i] * (epsRatio - 1) / ( epsRatio + 2) * pow(dotRadius / abs(x[i]), 3);
        }
    }
}

/*
 * Computes the time derivative of the state
 */
void TightBinding::operator() (const State &s, State &dsdt, const double t)
{
    int elecSize = getElectronSize();
    int holeSize = getHoleSize();
    int spaceSize = (elecSize - holeSize) / 2;

    // use lambdas to hide memory mapping
    auto elec = [=] (int a, int b) { return (a>=0 && b>=0 && a<elecSize && b<elecSize) ?  s[a*elecSize + b] : 0; };
    auto pol = [=] (int a, int b) { return (a>= 0 && b>=0 && a<elecSize && b<holeSize) ? s[elecSize*elecSize + a * holeSize + b] : 0; };
    auto hol = [=] (int a, int b) { return (a>=0 && b>=0 && a<holeSize && b<holeSize) ? s[(elecSize+holeSize)*elecSize + a*holeSize + b] : 0; };

    auto dElec = [&](int a, int b) -> std::complex<double>& {
        return dsdt[a*elecSize + b];
    };
    auto dPol = [&](int a, int b) -> std::complex<double>& {
        return dsdt[elecSize*elecSize + a*holeSize + b];
    };
    auto dHol = [&](int a, int b) -> std::complex<double>& {
        return dsdt[(elecSize+holeSize)*elecSize + a*holeSize + b];
    };
    double dSpill = 0;
    // This can be easily avoided by padding the c_e and c_h vectors with 0.
    auto wc_e = [=](int a) { return (a>=0 && a+1<elecSize) ? c_e[a] : 0; };
    auto wc_h = [=](int a) { return (a>=0 && a+1<holeSize) ? c_h[a] : 0; }; 

    double fj = param->dot.relaxation_fj;
    double fp = param->dot.relaxation_fp;
    std::complex<double> I = {0, 1.0};
    double E_field = E_laser(t, &param->pulse, param->calcAbsorption);
    // electron rates
    for(int i=0; i<elecSize; i++){
        double flag_hi = (i>=spaceSize && i<spaceSize+holeSize);
        for(int k=0; k<elecSize; k++){
            double flag_hk = (k>=spaceSize && k<spaceSize+holeSize);
            dElec(i, k) = -I * ( ( E_electron[k]-E_electron[i] - E_field*(V[k]-V[i])) * elec(i, k)
                                 + wc_e(k-1) * elec(i, k-1)
                                 + wc_e(k) * elec(i, k+1) 
                                 - wc_e(i-1) * elec(i-1, k)
                                 - wc_e(i) * elec(i+1, k)
                                 + E_field * dipolmoment * ( 
                                          std::conj(pol(i, k-spaceSize))
                                       -  pol(k, i-spaceSize) )
                               ) - 0.5 * fj *( flag_hi + flag_hk ) *(elec(i, k) - elec(k, i))
                          - std::max(absorber[i], absorber[k]) * elec(i, k);
        }
        dSpill += absorber[i] * std::abs(elec(i, i));
    }
    // hole rates
    for(int i=0; i<holeSize; i++){
        for(int k=0; k<holeSize; k++){
            dHol(i, k) = - I * ( ( E_hole[k] - E_hole[i] 
                                   + E_field * ( V[k+spaceSize] - V[i+spaceSize] ) ) * hol(i, k)
                                 + E_field * dipolmoment * (std::conj(pol(k+spaceSize, i)) - pol(i+spaceSize, k))
                                 + wc_h(k-1) * hol(i, k-1)
                                 + wc_h(k) * hol(i, k+1)
                                 - wc_h(i-1) * hol(i-1, k)
                                 - wc_h(i) * hol(i+1, k)
                               ) - fj * ( hol(i, k) - hol(k, i) );
        }
    }
    // polarization
    for(int i=0; i<elecSize; i++){
        double flag_hi = (i>=spaceSize && i<spaceSize+holeSize);
        for(int k=0; k<holeSize; k++){
            double flag_ik = (i == spaceSize+k);
            dPol(i, k) = - I * ( ( E_electron[i] + E_hole[k] + E_field * (V[k+spaceSize] - V[i])) * pol(i,k)
                                 + wc_e(i) * pol(i+1, k)
                                 + wc_e(i-1) * pol(i-1, k) 
                                 + wc_h(k-1) * pol(i, k-1)
                                 + wc_h(k) * pol(i, k+1)
                                 + E_field * dipolmoment * (flag_ik 
                                         - elec(k+spaceSize, i)
                                         - hol(i-spaceSize, k))
                              ) - 0.5 * fp * ( 1.0 + flag_hi) * pol(i, k)
                                - fj * flag_hi *( pol(i, k) - pol(k+spaceSize, i-spaceSize)) // pol relaxation 
                         - absorber[i] * pol(i, k);
        }
    }
    dsdt[(elecSize+holeSize)*elecSize + holeSize*holeSize] = dSpill;
}


const std::vector<double> & TightBinding::getX() const
{
    return x;
}

const double TightBinding::getDipolmoment() const
{
    return dipolmoment;
}

const std::vector<double> & TightBinding::getV() const
{
    return V;
}

const std::vector<double> & TightBinding::getC_e() const
{
    return c_e;
}

const std::vector<double> & TightBinding::getC_h() const
{
    return c_h;
}

