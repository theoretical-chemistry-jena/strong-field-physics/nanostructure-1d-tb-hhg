/*
 * Programmed by Martin Thuemmler
 * Friedrich Schiller University Jena
 * IPC - Group Graefe
 * Started 30.8.2022
 *
 *
 * Defines the adjustable input parameters
 *
 */

#ifndef PARAMETER_H
#define PARAMETER_H

#include<vector>
#include<math.h>

#include "unitConversion.h"

struct ParPulse_t {
    double lambda;
    double omega;
    double beta;
    double E0;
    double width;
    double cep;
    double start;
    double end;
};

struct ParPropagation_t {
    double startTime;
    double endTime;
    int outputCount;
    // maybe add something like integration method
};

struct ParGrid_t {
    double absorberSizeFactor; // in units of r_quiver
    double discretizationFactor; // = (dx * p_quiver) / ( PI)
    double spaceSizeFactor; // in units of r_quiver (for each half room)
    double absorberStrengthFactor; // multiplicant to theoretically minimal absober strength
    double dx;
    int absorberPot;
    double absorberStrength;
    int absorberSize;
    int spaceSize;
    int dotSize;
};

struct ParDot_t {
    double diameter;
    double siteDistance;
    double m_eff_hole;
    double m_eff_electron;
    double E_gap;
    double E_ionization;
    double epsilon_inside;
    double epsilon_outside;
    double dipolmoment;
    std::vector<double> relativeSurfaceDistance;
    double relaxation_fp;
    double relaxation_fj;
};

struct ParIO_t {
    bool verbose;
};

struct Par_t
{
    ParPulse_t pulse;
    ParPropagation_t propagation;
    ParGrid_t grid;
    ParDot_t dot;
    ParIO_t io;
    bool calcAbsorption;
};


void setup_default(Par_t * p);
void setup_default_io(ParIO_t * p);
void setup_default_pulse(ParPulse_t * p);
void setup_default_propagation(ParPropagation_t *p, const ParPulse_t *pulse);
void setup_CdSe_dot(ParDot_t * p);
void setup_default_grid(ParGrid_t * p);


double get_dt(const ParPropagation_t *p);

#endif
