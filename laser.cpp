#include "laser.h"

/* Defined using derivation of an A-field with gaussian shape */
double E_laser(double t, const ParPulse_t *p, bool calcAbsorption)
{
    if (calcAbsorption)
        return p->E0 * exp( -p->beta * t * t);
    
    double cphase = t * p->omega + p->cep;
    return p->E0 * ( cos(cphase) - 2* p->beta / p->omega * t * sin(cphase)) * exp(- p->beta *t*t);
}

void write_E_laser(Par_t *p)
{
    std::vector<double> t;
    t.resize(p->propagation.outputCount);
    double dt = get_dt(&(p->propagation));
    for(int i=0; i<t.size(); i++){
        t[i] = p->propagation.startTime + dt * i;
    }
    std::vector<double> E;
    E.resize(t.size());
    for(int i=0; i<t.size(); i++){
        E[i] = E_laser(t[i], &(p->pulse), p->calcAbsorption);
        t[i] = atomicUnits::to_fs(t[i]);
    }
    txtWriteVectors("laser.txt", "t [fs]\tE [a.u.]", t, E);
} 


