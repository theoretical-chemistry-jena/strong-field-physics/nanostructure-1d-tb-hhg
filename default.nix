{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
    nativeBuildInputs = [
                          pkgs.buildPackages.gcc11
                          pkgs.buildPackages.cmake
                          pkgs.buildPackages.boost
                          pkgs.buildPackages.fftw
                        ];
}

