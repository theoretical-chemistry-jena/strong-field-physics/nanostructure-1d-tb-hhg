#include "parameter.h"

void setup_default(Par_t * p)
{
    p->calcAbsorption = false;
    setup_default_io(&p->io);
    setup_default_pulse(&p->pulse);
    setup_default_propagation(&p->propagation, &p->pulse);
    setup_default_grid(&p->grid);
    setup_CdSe_dot(&p->dot);
}

void setup_default_io(ParIO_t * p)
{
    p->verbose = false;
}

void setup_default_pulse(ParPulse_t * p)
{
    p->lambda = atomicUnits::from_nm(5000);
    p->E0 = atomicUnits::from_V_nm(1.0);
    p->width = atomicUnits::from_fs(30);
    p->cep = M_PI / 2;
    p->start = - 3 * p->width;
    p->end = 3 * p->width;
}

void setup_default_propagation(ParPropagation_t *p, const ParPulse_t *pulse)
{
    p->startTime = pulse->start;
    p->endTime = pulse->end + 2 * pulse->width;
    p->outputCount = 10000;
}

void setup_CdSe_dot(ParDot_t * p) // for 1D CdSe
{
    p->diameter = atomicUnits::from_nm(2.2);
    p->siteDistance = atomicUnits::from_nm(0.4793); // pow(112, 1.0/3) / 10);
    p->m_eff_hole = 0.8;
    p->m_eff_electron = 0.13;
    p->E_gap = atomicUnits::from_eV(1.75);
    p->epsilon_inside = 2.45 * 2.45;
    p->epsilon_outside = 1;
    p->E_ionization = atomicUnits::from_eV(6.69);
    p->dipolmoment = 9.4; // 9.6244;
    p->relaxation_fp = 1 / atomicUnits::from_fs(1e100);
    p->relaxation_fj = 1 / atomicUnits::from_fs(1e100);
}

void setup_default_grid(ParGrid_t * p)
{
    p->absorberSizeFactor = 1; 
    p->absorberStrengthFactor = 3;
    p->absorberPot = 4;
    p->discretizationFactor = 0.2;
    p->spaceSizeFactor = 3;
}


double get_dt(const ParPropagation_t *p)
{
    return (p->endTime - p->startTime) / ( p->outputCount -1);
}


