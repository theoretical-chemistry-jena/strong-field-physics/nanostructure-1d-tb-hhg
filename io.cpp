#include "io.h"

char * normalize_input_line(char *line)
{
    if ( *line == '#')
        return nullptr;
    int k = 0;
    while(line[k] && line[k] != '=' && ! isspace(line[k]) )
        k++;
    if ( !line[k] )
        return nullptr;
    char * rhs = line + k;
    while ( *rhs != '=')
        rhs++;
    rhs++;
    while ( isspace(*rhs) )
        rhs++;
    char *end = rhs;
    while ( *end && !isspace(*end))
        end++;
    *end = 0;
    line[k] = 0;
    return rhs;
}

bool read_input_params(const char * filename, Par_t * p)
{
    FILE * f = fopen(filename, "r");
    char line[256];
    if ( ! f ){
        printf("Could not open input file %s\n", filename);
        return false;
    }
    bool pw = false, ps = false, pe = false, st = false, et = false;
    while ( fgets(line, sizeof(line), f) != nullptr)
    {
        char *rhs = normalize_input_line(line);
        if ( ! rhs ) 
            continue;
        if ( !strcmp(line, "calcAbsorption") )
            p->calcAbsorption = (atoi(rhs) != 0);

        if ( !strcmp(line, "lambda") )
            p->pulse.lambda = atomicUnits::from_nm(atof(rhs));
        if ( !strcmp(line, "E0") )
            p->pulse.E0 = atomicUnits::from_V_nm(atof(rhs));
        if ( !strcmp(line, "pulseWidth") )
        {
            p->pulse.width = atomicUnits::from_fs(atof(rhs));
            pw = true;
        }
        if ( !strcmp(line, "cep") )
            p->pulse.cep = atof(rhs);
        if ( !strcmp(line, "pulseStart") )
        {
            p->pulse.start = atof(rhs);
            ps = true;
        }
        if ( !strcmp(line, "pulseEnd") )
        {
            p->pulse.end = atof(rhs);
            pe = true;
        }
        if ( !strcmp(line, "startTime") ){
            p->propagation.startTime = atof(rhs);
            st = true;
        }
        if ( !strcmp(line, "endTime") ){
            p->propagation.endTime = atof(rhs);
            et = true;
        }
        if ( !strcmp(line, "outputCount") )
            p->propagation.outputCount = atof(rhs);
        
        if ( !strcmp(line, "absorberSizeFactor") )
            p->grid.absorberSizeFactor = atof(rhs);
        if ( !strcmp(line, "absorberPot") )
            p->grid.absorberPot = atoi(rhs);
        if ( !strcmp(line, "absorberStrengthFactor") )
            p->grid.absorberStrengthFactor = atof(rhs);
        if ( !strcmp(line, "discretizationFactor") )
            p->grid.discretizationFactor = atof(rhs);
        if ( !strcmp(line, "spaceSizeFactor") )
            p->grid.spaceSizeFactor = atof(rhs);
        
        if ( !strcmp(line, "dotDiameter") )
            p->dot.diameter = atomicUnits::from_nm(atof(rhs));
        if ( !strcmp(line, "Tp") )
            p->dot.relaxation_fp = 1 / atomicUnits::from_fs(atof(rhs));
        if ( !strcmp(line, "Tj") )
            p->dot.relaxation_fj = 1/ atomicUnits::from_fs(atof(rhs));
        for(unsigned i=0; i<10; i++){
            char rsf[50];
            char num[10];
            sprintf(num, "%d", i);
            strcpy(rsf, "relativeSurfaceDistance");
            strncat(rsf, num, 10);
            if ( !strcmp(line, rsf) ){
                unsigned currentSize = p->dot.relativeSurfaceDistance.size();
                if ( i>=currentSize ){
                    p->dot.relativeSurfaceDistance.resize(i+1);
                    for(unsigned int k=currentSize; k<i; k++)
                        p->dot.relativeSurfaceDistance[k] = 1;
                }
                p->dot.relativeSurfaceDistance[i] = atof(rhs);
            }
        }
        if ( !strcmp(line, "verbose") )
            p->io.verbose = (atoi(rhs) != 0);

    }
    if ( pw ){
        if (ps) p->pulse.start *=  p->pulse.width;
        if (pe) p->pulse.end *=  p->pulse.width;
        if (st) p->propagation.startTime *=  p->pulse.width;
        if (et) p->propagation.endTime *=  p->pulse.width;
    }
    fclose(f);
    return true;
}

void print_params(FILE *f, const Par_t * p)
{
    fprintf(f, "Pulse:\n");
    fprintf(f, "\tLambda: %.2f nm\n", atomicUnits::to_nm(p->pulse.lambda));
    fprintf(f, "\tE0_V_nm: %.2f nm\n", atomicUnits::to_V_nm(p->pulse.E0));
    fprintf(f, "\twidth: %.2f fs\n", atomicUnits::to_fs(p->pulse.width));
    fprintf(f, "\tcep: %.2f\n", p->pulse.cep);
    fprintf(f, "\tpulseStart: %.2f fs\n", atomicUnits::to_fs(p->pulse.start));
    fprintf(f, "\tpulseEnd: %.2f fs\n", atomicUnits::to_fs(p->pulse.end));

    fprintf(f, "Propagation\n");
    fprintf(f, "\tstartTime: %.2f fs\n", atomicUnits::to_fs(p->propagation.startTime));
    fprintf(f, "\tendTime: %.2f fs\n", atomicUnits::to_fs(p->propagation.endTime));
    fprintf(f, "\toutputCount: %d\n", p->propagation.outputCount);

    fprintf(f, "Grid\n");
    fprintf(f, "\tabsorberSizeFactor: %.2f\n", p->grid.absorberSizeFactor);
    fprintf(f, "\tabsorberStrengthFactor: %.2f\n", p->grid.absorberStrengthFactor);
    fprintf(f, "\tabsorberPot: %d\n", p->grid.absorberPot);
    fprintf(f, "\tabsorberSize: %d\n", p->grid.absorberSize);
    fprintf(f, "\tspaceSizeFactor: %.2f\n", p->grid.spaceSizeFactor);
    fprintf(f, "\tdiscretizationFactor: %.2f\n", p->grid.discretizationFactor);
    fprintf(f, "\tspaceSize: %d\n", p->grid.spaceSize);
    fprintf(f, "\tdotSize: %d\n", p->grid.dotSize);
    fprintf(f, "\tdx: %.4f\n", p->grid.dx);

    fprintf(f, "Dot\n");
    fprintf(f, "\tT_p %e fs\n", atomicUnits::to_fs(1/p->dot.relaxation_fp));
    fprintf(f, "\tT_j %e fs\n", atomicUnits::to_fs(1/p->dot.relaxation_fj));
    fprintf(f, "\trelativeSurfaceDistance:");
    for(unsigned i=0; i<p->dot.relativeSurfaceDistance.size(); i++)
        fprintf(f, " %.2f", p->dot.relativeSurfaceDistance[i]);
    fprintf(f, "\n");
}

