# IO:
writeLaser = 1
verbose = 1
calcAbsorption = 0




lambda = 5000 # nm
E0 = 1.5 # V/nm

dotDiameter = 2.2 # nm
Tp = 1e1 # fs
Tj = 1e1 # fs

pulseWidth = 30  # fs
pulseStart = -3 # in units of pulseWidth
pulseEnd = 3 # in units of pulseWidth
cep = 0 # 1.57

startTime = -3 # in units of pulseWidth
endTime = 6 # in units of pulseWidth
outputCount = 5000

absorberSizeFactor = 1 # in units of r_quiver
absorberPot = 4
absorberStrengthFactor = 2
discretizationFactor = 0.4 # in units of pi/r_quiver
spaceSizeFactor = 2.5 # in units of r_quiver

