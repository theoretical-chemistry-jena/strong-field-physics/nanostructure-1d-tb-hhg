#ifndef LASER_H
#define LASER_H

#include<vector>

#include "parameter.h"
#include "io.h"


double E_laser(double t, const ParPulse_t *p, bool calcAbsorption);
void write_E_laser(Par_t *p);

#endif
