/*
 * Programmed by Martin Thuemmler
 * Friedrich Schiller University Jena
 * IPC - Group Graefe
 * Started 30.8.2022
 *
 *
 * Implements a 1D tight binding model for CdSe to calculate HHG spectra
 * For more information see the readme file.
 *
 */




#include <ctype.h>
#include <math.h>
#include <vector>
#include <random>

#include <complex>
#include <boost/numeric/odeint.hpp>
#include <boost/mpl/min_max.hpp>
#include <boost/math/quadrature/trapezoidal.hpp>

#include<fftw3.h>
#include<complex>

#include "parameter.h"
#include "io.h"
#include "tightBinding.h"
#include "stateEvaluator.h"



/*
 * Wrapper function for fftw to calculate fast fourier transform
 */
std::vector<std::complex<double>> dft_r2c_1d(const std::vector<double> &data)
{
    double * in = (double*) fftw_malloc(sizeof(double) * data.size());
    std::vector<std::complex<double>> res;
    res.resize(data.size()/2 + 1);
    fftw_plan fplan = fftw_plan_dft_r2c_1d(data.size(), in, (fftw_complex*)(&res[0]), FFTW_ESTIMATE);
    for(int i=0; i<data.size(); i++)
       in[i] = data[i];
    fftw_execute(fplan);
    fftw_destroy_plan(fplan);
    fftw_free(in);
    return res;
}




/***************************************/

/*
 * value: of decaying function in (equidistant sampled) time domain
 * exciation: time evolution of exciting / probing pulse
 */
std::vector<double> calcAbsorption(const std::vector<double> &value, const std::vector<double> &excitation)
{
    assert(value.size() == excitation.size());
    auto fft_value = dft_r2c_1d(value);
    auto fft_exc = dft_r2c_1d(excitation);
    std::vector<double> res(fft_value.size());
    for(int i=0; i<res.size(); i++)
        res[i] = std::imag(fft_value[i] / fft_exc[i]);
    return res;
}




/** Initialization stuff **/

void initialize_pulse(ParPulse_t *p)
{
    p->omega = 2 * M_PI * atomicUnits::speedOfLight /  p->lambda;
    p->beta = 2 * log(2) / p->width / p->width;
}

void initialize_position(ParGrid_t * g, std::vector<double> &x, double a, 
                         const std::vector<double> & rsd)
{
    if ( g->dotSize/2 < rsd.size() + 1)
        printf("WARNING: half of the dot smaller than surface\n");

    auto relDistance = [&] (int id) {
        int dotId = id - g->spaceSize;
        int surfId = dotId <= g->dotSize/2-1 ? dotId : g->dotSize-2-dotId;
        if ( surfId < 0 || surfId >= rsd.size())
            return 1.0;
        return rsd[surfId];
    };

    x.resize(g->dotSize + 2 * g->spaceSize);
    double relSize = 0;
    for(int i=g->spaceSize; i<g->spaceSize + (g->dotSize-1)/2; i++)
        relSize += relDistance(i);
    if ( g->dotSize %2 == 0)
        relSize += relDistance(g->spaceSize + g->dotSize/2 -1) / 2;
    double pos = - (g->spaceSize) * g->dx - a * relSize;
    for(int i=0; i<g->spaceSize; i++){
        x[i] = pos;
        pos += g->dx;
    }
    for(int i=g->spaceSize; i<g->spaceSize + g->dotSize-1; i++){
        x[i] = pos;
        pos += a * relDistance(i);
    }
    x[g->spaceSize + g->dotSize-1] = pos;
    for(int i=g->spaceSize+g->dotSize; i<2*g->spaceSize+g->dotSize; i++){
        pos += g->dx;
        x[i] = pos;
    }
}

void initialize_absorber(ParGrid_t * g, std::vector<double> &absorber, double p_max ){
    absorber.resize(g->dotSize + 2 * g->spaceSize);
    for(int i=0; i<absorber.size(); i++)
        absorber[i] = 0;
    if ( !g->absorberSize ) {
        printf("WARNING: no absorber set.\n");
        g->absorberStrength = 0;
    } else {
        double eps = 1e-15;
        int ap = g->absorberPot;
        int as = g->absorberSize;
        double minAbsorberStrength = - (ap+1) * p_max / 2 / pow((as-1), ap+1) / g->dx * log(eps);
        g->absorberStrength = g->absorberStrengthFactor * minAbsorberStrength;
        for(int i=0; i<as; i++){
            double val = g->absorberStrength * pow((i+1), ap);
            absorber[as-i-1] = val;
            absorber[absorber.size() - (as-i)] = val;
        }
    }
}

void initialize_grid(Par_t * p, std::vector<double> &x, std::vector<double> &absorber)
{
    double a = p->dot.siteDistance;
    p->grid.dotSize = 1 + (p->dot.diameter / a + 0.5) ;

    double p_quiver = p->pulse.E0 / p->pulse.omega;
    double r_quiver = p->pulse.E0 / p->pulse.omega / p->pulse.omega;

    double dx = p->grid.discretizationFactor * M_PI / p_quiver;
    p->grid.dx = dx;
    double spaceOutside = p->grid.spaceSizeFactor * r_quiver;
    p->grid.spaceSize = std::max(spaceOutside / dx + 0.5, 5.0);
    
    p->grid.absorberSize = std::max(p->grid.absorberSizeFactor * r_quiver / dx + 0.5, 2.0);

    initialize_position(&(p->grid), x, p->dot.siteDistance, p->dot.relativeSurfaceDistance);
    initialize_absorber(&(p->grid), absorber, p_quiver);
}

/** EOM integration using BOOST **/

class ObserverTBI {
    StateEvaluator & stateEval;
    int callCount;
    int maxCallCount;
    clock_t startTime;
public:
    ObserverTBI(StateEvaluator & stateEval, int maxCallCount);
    void operator() (const State &s, double tc);
};

ObserverTBI::ObserverTBI(StateEvaluator & stateEval, 
                         int maxCallCount) : stateEval(stateEval), maxCallCount(maxCallCount)
{
    callCount = 0;
    startTime = clock();
}

void ObserverTBI::operator()(const State &s, double tc){
    callCount++;
    int fact = 10;
    int mod = maxCallCount / fact;
    if ( stateEval.getParam()->io.verbose && callCount % mod == 0){
        clock_t currentTime = clock();
        int sec = (currentTime - startTime)/ CLOCKS_PER_SEC;
        printf("%d/%d \tTime: %0d:%02d:%02d\n", callCount / mod, fact, sec/3600, (sec/60)%60, sec%60 );
    }
    stateEval.addState(tc, s);
}

StateEvaluator integrate_tb(const TightBinding *tb)
{
    // setup parameters -- initial state has no hole and no electron
    int elecSize = tb->getElectronSize();
    int holeSize = tb->getHoleSize();
    State state(elecSize * elecSize + elecSize * holeSize + holeSize * holeSize + 1, 0);
    double start = tb->getParam()->propagation.startTime;
    double end = tb->getParam()->propagation.endTime;
    int count = tb->getParam()->propagation.outputCount;
    double dt = get_dt( &tb->getParam()->propagation );
    if ( tb->getParam()->io.verbose)
        printf("State size: %d\n", int(state.size()));
    StateEvaluator stateEval(tb);
    ObserverTBI obs(stateEval, count);
    // perform integration
    int stepCount = boost::numeric::odeint::integrate_const(
            boost::numeric::odeint::make_controlled < 
                        boost::numeric::odeint::runge_kutta_cash_karp54< State > >(1e-10, 1e-10),
                        *tb, state, start, end, dt, obs);
    if ( tb->getParam()->io.verbose)
        printf("Required %d steps\n", stepCount);
    return stateEval;
}


std::pair<StateEvaluator, TightBinding> perform_tb_simulation(Par_t * param)
{
    std::vector<double> x, absorber;
    initialize_pulse(&(param->pulse));
    initialize_grid(param, x, absorber);
    if ( param->io.verbose )
        print_params(stdout, param);
    write_E_laser(param);
    TightBinding tb(param, x, absorber);
    return std::pair(integrate_tb(&tb), tb);
}

void calc_spectral_absorption(Par_t * p)
{
    const double maxEv = 30;
    auto tbs = perform_tb_simulation(p);
    auto se = tbs.first;
    const auto pol = se.getPolPol();
    std::vector<double> probeField(pol.size());
    double dt = get_dt(&p->propagation);
    double t = p->propagation.startTime;
    for(int i=0; i<p->propagation.outputCount; i++){
        probeField[i] = E_laser(t, &p->pulse, p->calcAbsorption);
        t += dt;
    }
    const auto absorption = calcAbsorption(pol, probeField);
    std::vector<double> eVscale(absorption.size());
    double maxAbsorption = 0;
    for(int i=0; i<absorption.size(); i++){
        eVscale[i] = i * 2*M_PI / dt * atomicUnits::to_eV(1) / pol.size();
        if ( eVscale[i] < maxEv && absorption[i] > maxAbsorption)
            maxAbsorption = absorption[i];
    }
    txtWriteVectors("absorption.txt", "E [eV]\tAbsorption [arb. unit]", eVscale, absorption);
}

/******************************/

int main(int argc, char *argv[])
{
    Par_t param;
    memset(&param, 0, sizeof(Par_t));
    setup_default(&param);
    if ( argc > 1){
        if ( ! read_input_params(argv[1], &param))
            return 0;
    } else {
        if ( ! read_input_params("input.txt", &param))
            return 0;
    }
    if ( param.calcAbsorption ){
        calc_spectral_absorption(&param);
        return 0;
    }

    auto tbs = perform_tb_simulation(&param);
    auto se = tbs.first;
    se.write();
    return 0;
}
