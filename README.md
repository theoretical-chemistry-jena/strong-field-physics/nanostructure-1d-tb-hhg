#Two-Particle Tight-Binding Description of Higher-Harmonic Generation in Semiconductor Nanostructures
## Authors
Ulf Peschel, Thomas Lettau, Stefanie Gräfe

    Friedrich Schiller University Jena
    Max-Wien-Platz 1, 07743 Jena, Germany
Kurt Busch

    Humboldt-Universität zu Berlin, AG Theoretische Optik & Photonik, Newtonstr. 15, 12489 Berlin,
    Germany, and
    Max-Born-Institut, Max-Born-Str. 2A, 12489 Berlin, Germany

## Application
The provided C++ source code implements the described model published in ***TBD***.

Required Libraries:

* Boost ( >= 1.77)
* fftw3 ( >= 3.3.10)

## Build and usage
To build the program, create a build folder and run cmake inside.
Afterwards, you can compile the program using make.
The input parameters of the program can be modified in *input.txt*

    usage (in build directory): ./main ../input.txt

The program generates self-describing text files.
Examples of input files are given in
*input_hhg.txt* and *input_absorb.txt*.
