/*
 * Programmed by Martin Thuemmler
 * Friedrich Schiller University Jena
 * IPC - Group Graefe
 * Started 30.8.2022
 *
 *
 * Very simplistic implementation of parameter IO and file output
 *
 */


#ifndef IO_H
#define IO_H

#include <stdio.h>
#include <ctype.h>
#include <cstring>

#include "parameter.h"


template<typename T>
void __txtWriteVectorsLine(FILE *f, int i, const T v)
{
    fprintf(f, "%g\n", v[i]);
}

template<typename T1, typename T2, typename... Types>
void __txtWriteVectorsLine(FILE *f, int i, const T1 v1, const T2 v2, Types... vecs)
{
    fprintf(f, "%g\t", v1[i]);
    __txtWriteVectorsLine(f, i, v2, vecs...);
}


/*
 * These template function enables easy writing of double arrays to files, which can 
 * be read for instance by numpy
 */
template<typename T, typename... Types>
bool txtWriteVectors(const char * filename, const char * header,
                     const T vec, const Types... vecs)
{
    FILE * f = fopen(filename, "w");
    if ( !f )
        return false;
    fprintf(f, "# %s\n", header);
    for(unsigned int i=0; i<vec.size(); i++)
        __txtWriteVectorsLine(f, i, vec, vecs...);
    fclose(f);
    return true;
}


char * normalize_input_line(char *line);
bool read_input_params(const char * filename, Par_t * p);
void print_params(FILE *f, const Par_t * p);

#endif
