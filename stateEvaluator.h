/*
 * Programmed by Martin Thuemmler
 * Friedrich Schiller University Jena
 * IPC - Group Graefe
 * Started 30.8.2022
 *
 *
 * Calculates desired observables from a given TB state
 * Enables writing of the calculated observables for all the collected times
 */


#ifndef STATE_EVALUATOR_H
#define STATE_EVALUATOR_H

#include "tightBinding.h"
#include "io.h"

typedef std::pair<std::vector<double>, std::vector<std::complex<double>>> SpectralPair;
class StateEvaluator {
    const TightBinding * tb;
    std::vector<double> t;
    std::vector<double> eSum;
    std::vector<double> hSum;
    std::vector<double> spill;
    std::vector<double> elec;
    std::vector<double> hole;
    std::vector<double> elecMeanPos;
    std::vector<double> holeMeanPos;
    std::vector<double> polPol;
    std::vector<double> polCurrent;
public:
    const Par_t * getParam() const;
    StateEvaluator(const TightBinding * tb);
    void addState(double tc, const State &s);
    const std::vector<double> & getPolPol() const;
    const std::vector<double> & getPolCurrent() const;
    void write();
};

#endif
