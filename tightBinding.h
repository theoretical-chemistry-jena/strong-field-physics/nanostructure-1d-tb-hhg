/*
 * Programmed by Martin Thuemmler
 * Friedrich Schiller University Jena
 * IPC - Group Graefe
 * Started 30.8.2022
 *
 *
 * Implements Tightbinding Hamiltonian
 * Calculates time derivative on a linearized state vector
 *  (electron correlation, hole correlation, polarization)
 * 
 */


#ifndef TIGHT_BINDING_H
#define TIGHT_BINDING_H

#include<vector>
#include<complex>

#include "parameter.h"
#include "laser.h"


typedef std::vector< std::complex<double> > State;

class TightBinding
{
    std::vector<double> E_electron;
    std::vector<double> E_hole;
    std::vector<double> V;
    double dipolmoment;
    std::vector<double> c_e; // c_e[i] couples i to i+1
    std::vector<double> c_h; // c_h[i] couples i to i+1
    const std::vector<double> absorber;
    const std::vector<double> x;
    const Par_t * param;
public:
    TightBinding(const Par_t *p, const std::vector<double> x,
                   const std::vector<double> absorber);
    int getElectronSize() const { return E_electron.size(); } 
    int getHoleSize() const { return E_hole.size(); }
    const Par_t * getParam() const { return param; }
    const std::vector<double> & getX() const;
    const std::vector<double> & getV() const;
    const std::vector<double> & getC_e() const;
    const std::vector<double> & getC_h() const;
    const double getDipolmoment() const;
    void operator() ( const State &s, State &dsdt, const double t);
};

#endif
