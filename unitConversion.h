/*
 * Programmed by Martin Thuemmler
 * Friedrich Schiller University Jena
 * IPC - Group Graefe
 * Started 30.8.2022
 *
 *
 * Defines unit conversion functions from and to atomic units
 *
 */

#ifndef UNIT_CONVERSION_H
#define UNIT_CONVERSION_H

namespace atomicUnits {
      // time
    inline double from_as(double v) { return v / 24.19; }
    inline double to_as(double v) { return v * 24.19; }

    inline double from_fs(double v) { return v / 0.02419; }
    inline double to_fs(double v) { return v * 0.02419; }


    // length
    inline double from_A(double v) { return v / 0.529; }
    inline double to_A(double v) { return v * 0.529; }

    inline double from_nm(double v) { return v / 0.0529; }
    inline double to_nm(double v) { return v * 0.0529; }

    // intensity
    inline double from_W_cm2(double v) { return v / 3.51e16; }
    inline double to_W_cm2(double v) { return v * 3.51e16; }

    // field strength
    inline double from_V_nm(double v) { return v / 514.2; }
    inline double to_V_nm(double v) { return v * 514.2; }

    // energy
    inline double from_eV(double v) { return v / 27.211; }
    inline double to_eV(double v) { return v * 27.211; }

    const double speedOfLight = 137.036;
};

#endif
