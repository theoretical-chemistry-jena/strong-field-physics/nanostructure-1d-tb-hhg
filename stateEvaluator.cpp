#include "stateEvaluator.h"


StateEvaluator::StateEvaluator(const TightBinding *tb) : tb(tb) {}


const Par_t * StateEvaluator::getParam() const
{
    return tb->getParam();
}

const std::vector<double> & StateEvaluator::getPolPol() const
{
    return polPol;
}

const std::vector<double> & StateEvaluator::getPolCurrent() const
{
    return polCurrent;
}

void StateEvaluator::addState(double tc, const State &s)
{
    int elecSize = tb->getElectronSize();
    int holeSize = tb->getHoleSize();
    int spaceSize = (elecSize - holeSize) / 2;
    // define lambdas to hide memory mapping
    auto selec = [=] (int a, int b) { return (a>=0 && b>=0 && a<elecSize && b<elecSize) ? s[a*elecSize + b] : 0; };
    auto spol = [=] (int a, int b) { return (a>=0 && b>=0 && a<elecSize && b<holeSize) ? s[elecSize*elecSize + a * holeSize + b] : 0; };
    auto shol = [=] (int a, int b) { return (a>=0 && b>=0 && a<holeSize && b<holeSize) ? s[(elecSize+holeSize)*elecSize + a*holeSize + b] : 0; };
    double cSpill = std::real(s[(elecSize+holeSize)*elecSize + holeSize * holeSize]);

    auto & x = tb->getX();
    t.push_back(tc);
    double elecSum = 0;
    double elecMean = 0;
    for(int i=0; i<elecSize; i++){
        double e = std::abs(selec(i, i));
        elec.push_back(e);
        elecSum += e;
        elecMean += e * x[i];
    }
    elecMeanPos.push_back(elecMean / elecSum);
    eSum.push_back(elecSum);
    spill.push_back(cSpill);
    double holeMean = 0;
    double holeSum = 0;
    for(int i=0; i<holeSize; i++){
        double h = std::abs(shol(i, i));
        holeSum += h;
        holeMean += h * x[i+spaceSize];
        hole.push_back(h);
    }
    hSum.push_back(holeSum);
    holeMeanPos.push_back(holeMean / holeSum);
    
    // for spectrum
    const auto & V = tb->getV();
    double pPol = 0;
    double pV = 0;
    for(int i=0; i<holeSize; i++){
        pPol += 2 * tb->getDipolmoment() * std::real(spol(spaceSize+i, i));
        pV += V[spaceSize+i]*( -std::abs(selec(spaceSize+i, spaceSize+i)) + std::abs(shol(i, i)));
    }
    for(int i=0; i<spaceSize; i++){
        int k = spaceSize + holeSize + i;
        pV -= V[i] * std::abs(selec(i, i)) + V[k] * std::abs(selec(k, k));
    } 
    polPol.push_back(pPol+pV);
    double polCurrentSum = 0;
    const auto & c_e = tb->getC_e();
    const auto & c_h = tb->getC_h();
    double a = tb->getParam()->dot.siteDistance;
    double dx = tb->getParam()->grid.dx;
    
    int i = 0;
    int ie = i + spaceSize;
    double j = 0;
    for(i=0; i<holeSize-1; i++){
        ie = i+spaceSize;
        j += a *( c_h[i] * std::imag(shol(i, i+1) - shol(i, i-1))
                 - c_e[ie] * std::imag(selec(ie, ie+1) - selec(ie, ie-1)) );
    }
    for(int i=0; i<spaceSize-1; i++){
        int k = i + spaceSize + holeSize;
        j -= dx * c_e[k] * std::imag( selec(k, k+1) - selec(k, k-1) );
        j -= dx * c_e[k] * std::imag( selec(i, i+1) - selec(i, i-1) );
    }
    polCurrent.push_back( j );
}

void StateEvaluator::write()
{
    txtWriteVectors("chargeSum.txt", "t[a.u.]\tElectrons\tHoles\tSpill", t, eSum, hSum, spill);
    txtWriteVectors("dipol.txt", "t [a.u.]\tpolarization\tcurrent", t, polPol, polCurrent);
}

